const array = ["1", "2", "3", "sea", "user", 23];
const parent = document.querySelector("ul");

const createList = (array, parent) => {
  let list = "";
  array.map((e) => {
    list += `<li>${e}</li>`;
  });
  parent.innerHTML = list;
};

createList(array, parent);
